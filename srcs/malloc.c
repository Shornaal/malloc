#include "malloc.h"

t_maps		g_maps = {NULL, NULL, NULL, 0, 0, 0};

void		set_malloc_error(void)
{
	errno = ENOMEM;
	return ;
}

void			init_maps(void)
{
	struct rlimit	m_rlimit;

	g_maps.size_tiny = TINY;
	g_maps.size_small = SMALL;
	if (!getrlimit(RLIMIT_DATA, &m_rlimit))
		g_maps.max_size = m_rlimit.rlim_cur;
}

void			*create_block(int bsize, size_t size, t_block *block, void *end)
{
	t_block		*tmp;
	size_t		length;

	tmp = NULL;
	length = 0;
	if ((unsigned int)bsize > g_maps.size_tiny)
		length = size + g_maps.size_tiny + sizeof(t_block);
	else
		length = size + sizeof(t_block);
	if ((unsigned int)bsize > g_maps.size_small || block->next < length)
		size = block->next;
	else
	{
		tmp = (void *)block + size;
		tmp->prev = size;
		tmp->next = block->next - size;
		tmp->flag = 0;
		if ((void *)tmp + tmp->next + sizeof(t_block) < end)
		{
			tmp = (void *)tmp + tmp->next;
			tmp->prev = block->next - size;
		}
	}
	block->next = size;
	block->flag = 1;
	return (block);
}

static void		*find_free_space(int bsize, size_t size, void **map)
{
	t_header	*header;
	t_block		*block;

	header = NULL;
	block = NULL;
	header = (t_header *)*map;
	while (header)
	{
		block = (t_block *)header->first;
		while (block && (void *)block + block->next <= header->end)
		{
			if (block->flag == 0 && block->next >= size)
				return (create_block(bsize, size, block, header->end));
			if ((void *)block + block->next == header->end)
				block = NULL;
			else
				block = (void *)block + block->next;
		}
		if (!header->next)
		{
			if (create_map(bsize, map, header) < 0)
			{
				set_malloc_error();	
				return (NULL);
			}
		}
		else
			header = header->next;
	}
	set_malloc_error();
	return (NULL);
}

void	*malloc(size_t size)
{
	void			*ptr;

	// ft_putstr("[HOOK] Malloc called of size : ");
	// ft_putnbr(size);
	// ft_putchar('\n');

	ptr = NULL;
	if (size <= 0)
		size = 1;	
	if (!g_maps.tiny && !g_maps.small && !g_maps.large)
		init_maps();
	if (size + sizeof(t_header) > g_maps.max_size || size > g_maps.max_size)
		   return (NULL);	
	size = size + sizeof(t_block);
	if (size - sizeof(t_block) <= g_maps.size_tiny)
	{
		if (!g_maps.tiny && create_map(g_maps.size_tiny, &g_maps.tiny, NULL) < 0)
				return (NULL);
		ptr = find_free_space(g_maps.size_tiny, size, &g_maps.tiny);
	}
	else if (size - sizeof(t_block) <= g_maps.size_small)
	{
		if (!g_maps.small && create_map(g_maps.size_small, &g_maps.small, NULL) < 0)
				return (NULL);
		ptr = find_free_space(g_maps.size_small, size, &g_maps.small);
	}
	else
		ptr = large_alloc(size, &g_maps.large);

	/*
	 * @args size min value = 1 || size.
	 * @return ptr = addr of free chunks.
	 * 1. Initialise all maps with global / singleton.
	 * 2. If size + sizeof(header) > max_size, error return null ptr.
	 * 3. Add to size size of one block.
	 * 4. Switch with (<=) between TINY, SMALL or LARGE maps.
	*/
	if (ptr)
		ptr = (void *)ptr + sizeof(t_block);
	if (!g_maps.tiny)
		ft_putstr("NO TINY\n");
	if (!g_maps.small)
		ft_putstr("NO SMALL\n");
	show_alloc_mem();
	return (ptr);
}
