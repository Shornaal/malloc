/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 20:18:03 by tiboitel          #+#    #+#             */
/*   Updated: 2016/02/24 19:38:08 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static size_t		check_format_integrity(size_t size)
{
		if (size && size <= TINY)
			return (TINY);
		if (size > TINY + 1 && size <= SMALL)
			return (SMALL);
		return (0);
}

static void		*swap_block(t_block *block, t_header *header, size_t size)
{
	void		*tmp;
	size_t		block_length;

	tmp = NULL;
	block_length = sizeof(t_block);
	if (block->next - block_length <= TINY)
		tmp = create_block(TINY, size + block_length, block, header->end);
	else if (block->next - block_length <= SMALL)
		tmp = create_block(SMALL, size + block_length, block, header->end);
	else
		return (NULL);
	return (tmp + block_length);
}

void			*realloc(void *ptr, size_t size)
{
	void		*new_ptr;
	t_header	*header;
	t_block		*index;
	
	if (!ptr && !size)
		return (NULL);
	if (!ptr)
		return (malloc(size));
	if (!size)
		size = 1;
	index = ptr - sizeof(t_block);
	if (!(header = get_header_type(index)) 
		&& !(check_pointer_integrity(header, index)))
			return (NULL);
	if (header->format != check_format_integrity(size) 
			|| size + sizeof(t_block) > index->next)
	{
		new_ptr = malloc(size);
		if (size > index->next - sizeof(t_block))
			size = index->next - sizeof(t_block);
		ft_memcpy(new_ptr, ptr, size);
		free(ptr);
		return (new_ptr);
	}
	return (swap_block(index, header, size));	
}
