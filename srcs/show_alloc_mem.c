/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler_display.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 16:38:51 by tiboitel          #+#    #+#             */
/*   Updated: 2016/02/24 20:46:43 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void			display_area_type(t_header *header, t_area_mem type)
{
	if (type == A_TINY)
		ft_putstr("TINY");
	else if (type == A_SMALL)
		ft_putstr("SMALL");
	else
		ft_putstr("LARGE");
	ft_putstr(" : ");
	ft_puthex((size_t)header);
	ft_putchar('\n');
}

static void			display_handler_fetch(t_header *header, t_area_mem type)
{
	t_block		*index;

	while (header)
	{
		index = header->first;
		while (index)
		{
			if (index->flag)
			{
				ft_puthex(((size_t)(void *)index + sizeof(t_block)));
				ft_putstr(" - ");
				ft_puthex(((size_t)(void *)index + index->next));
				ft_putstr(" : ");
				ft_putnbr(index->next - sizeof(t_block));
				ft_putchar('\n');
			}
			index = (void *)index + index->next;
			if ((void *)index + 1 >= header->end)
				index = NULL;
		}
		header = header->next;
	}
}

static void			display_handler_query(t_area_mem type)
{
	if (type == A_TINY && g_maps.tiny)
	{
		display_area_type((t_header *)g_maps.tiny, type);
		display_handler_fetch((t_header *)g_maps.tiny, type);
	}
	if (type == A_SMALL && g_maps.small)
	{
		display_area_type((t_header *)g_maps.small, type);
		display_handler_fetch((t_header *)g_maps.small, type);
	}
	if (type == A_LARGE && g_maps.large)
	{
		display_area_type((t_header *)g_maps.large, type);
		display_handler_fetch((t_header *)g_maps.large, type);
	}
}

static void		display_handler_fetch_all(void)
{
	t_area_mem		type;

	type = A_TINY;
	while (type <= A_LARGE)
	{
		display_handler_query(type);
		type++;
	}
}

void			show_alloc_mem(void)
{
	display_handler_fetch_all();
}
