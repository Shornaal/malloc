/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 18:16:41 by tiboitel          #+#    #+#             */
/*   Updated: 2016/02/24 19:52:32 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	*calloc(size_t count, size_t size)
{
	void 	*ptr;
	size_t	new_size;

	//ft_putstr("[HOOK] Calloc called !\n");
	if (count && size && 65535 / count < size)
	{
		errno = ENOMEM;
		return (NULL);
	}
	new_size = count * size;
	if ((ptr = malloc(new_size)) == NULL)
		return (NULL);
	ft_bzero(ptr, new_size);
	return (ptr);
}
