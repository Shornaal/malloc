/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 17:21:34 by tiboitel          #+#    #+#             */
/*   Updated: 2016/02/24 19:28:17 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void	free_ptr(t_header *head, t_block *ptr, t_block *end, size_t len)
{
	if ((ptr == head->first && end) || (ptr != head->first && end))
	{
		ptr->next = len;
		end->prev = len;
		ptr->flag = 0;
		ft_bzero((void *)ptr + sizeof(t_block), len - sizeof(t_block));
		g_maps.max_size += len;
	}
	else if (ptr != head->first && !end)
	{
		ptr->next = len;
		ptr->flag = 0;
		ft_bzero((void *)ptr + sizeof(t_block), len - sizeof(t_block));
		g_maps.max_size += len;
	}
	else if (ptr == head->first && !end)
	{
		ptr->next = head->size - sizeof(t_header) - 1;
		ptr->flag = 0;
		if (head->prev || head->next)
			destroy_map(head);
	}
	return ;
}

int			check_pointer_integrity(t_header *header, t_block *block)
{
	t_block		*tmp;

	tmp = header->first;
	while ((void *)tmp < header->end)
	{
		if (tmp == block)
			return (1);
		tmp = (void *)tmp + tmp->next;
	}
	return (0);
}

t_header		*get_header_type(t_block *block)
{
	t_header	*header;
	t_area_mem	curr;

	header = NULL;
	curr = A_TINY;
	while (curr < A_LARGE)
	{
		if (curr == A_TINY)
			header = (t_header *)(g_maps.tiny);
		else if (curr == A_SMALL)
			header = (t_header *)(g_maps.small);
		else
			header = (t_header *)(g_maps.large);
		if (!header)
		{
			//ft_putstr("[ERROR] Missing header.\n");
			return (NULL);
		}
		if (!header->end)
		{
			//ft_putstr("[ERROR] Missing end of header.\n");
			return (NULL);
		}
		while (header)
		{
			if ((void *)block > (void *)header && (void *)block < header->end)
				return (header);
			header = header->next;
		}
		curr++;
	}
	return (NULL);
}

static void		defragment_virtual_mem(t_header *header, t_block *block)
{
	t_block		*index;
	size_t		size;

	block->flag = 0;
	index = block;
	while (!index->flag && index->prev)
		index = (void *)index - index->prev;
	if (!index->flag)
		block = index;
	else
		block = (void *)index + index->next;
	index = block;
	size = 0;
	while (!index->flag)
	{
		size += index->next;
		index = (void *)index + index->next;
		if ((void *)index >= header->end)
			return (free_ptr(header, block, NULL, size));
	}
	return (free_ptr(header, block, index, size));
}

void	free(void *ptr)
{
	t_header	*header;
	t_block		*curr;

	// ft_putstr("[HOOK] Free called !\n");
	if (!ptr)
		return;
	curr = (void *)ptr - sizeof(t_block);
	if (!(header = get_header_type(curr)) || !(check_pointer_integrity(header, curr)))
	{
		set_malloc_error();
		return ;	
	}
	(header->format > SMALL) ? free_large(header, curr) :
		defragment_virtual_mem(header, curr);
	return ;
}
