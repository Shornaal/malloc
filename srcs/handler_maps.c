#include "malloc.h"

static t_block	*init_first_block(t_header *header, size_t size)
{
	t_block		*first;

	first = (void *)header + sizeof(t_header);
	first->next = size - sizeof(t_header) - 1;
	first->prev = 0;
	first->flag = 0;
	return (first);
}

int		create_map(int bsize, void **maps, t_header *prev)
{
	size_t		size;
	t_header	*header;

	size = (bsize + sizeof(t_block)) * 100 + sizeof(t_header);
	size = size + (getpagesize() - (size % getpagesize()));
	if (size > g_maps.max_size)
	{
		set_malloc_error();
		return (-1);
	}
	*maps = mmap(0, size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANON | MAP_PRIVATE, -1, 0);
	if (*maps == MAP_FAILED)
	{
		set_malloc_error();
		return (-1);
	}
	g_maps.max_size -= size;
	header = create_header(maps, size, prev, bsize);
	if (prev)
		prev->next = header;
	return (0);
}

void		destroy_map(t_header *header)
{
	if (header->prev)
		header->prev->next = header->next;
	else
	{
		if (header == g_maps.tiny)
			g_maps.tiny = (void *)header->next;
		else if (header == g_maps.small)
			g_maps.small = (void *)header->next;
		else if (header  == g_maps.large)
			g_maps.large = (void *)header->next;
	}
	if (header->next)
		header->next->prev = header->prev;
	ft_bzero((void *)header, header->size);
	munmap((void *)header, header->size);
}

t_header	*create_header(void **map, size_t size, t_header *prev, int bsize)
{
	t_header	*header;

	header = (t_header *)*map;
	header->prev = prev;
	header->next = NULL;
	header->end = (void *)header + size - 1;
	header->size = size;
	header->format = bsize;
	header->first = init_first_block(header, size);
	return (header);
}

