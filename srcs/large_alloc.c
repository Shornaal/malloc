/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   large_alloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 18:28:11 by tiboitel          #+#    #+#             */
/*   Updated: 2016/02/24 18:48:48 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "malloc.h"

void			*set_large_block(t_header *header, size_t size)
{
	t_block		*block;

	header->next = NULL;
	header->size = size;
	header->end = (void *)header + header->size - 1;
	header->format = size;
	block = (void *)header + sizeof(t_header);
	block->prev = 0;
	block->next = header->size - sizeof(t_header) - 1;
	block->flag = 1;
	header->first = block;
	return (block);
}

void			free_large(t_header	*header, t_block *block)
{
	while (header)
	{
		if (block && block != header->first)
			header = header->next;
		else
		{
			destroy_map(header);
			return ;
		}
	}
}

void			*large_alloc(size_t size, void **map)
{
	// Declare une map multiple de getpagesize()
	// Soustraite taille a g_maps max_size.
	t_header	*large;
	t_header	*header;

	size += sizeof(t_header);
	size += (getpagesize() - (size % getpagesize()));
	// claim new map function ?
	large = mmap(0, size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANON | MAP_PRIVATE, -1, 0);
	if (large == MAP_FAILED)
		return (NULL);
	g_maps.max_size -= size;
	if (*map == NULL)
	{
		*map = (void *)large;
		large->prev = NULL;
	}
	else
	{
		header = (t_header *)*map;
		while (header->next)
			header = header->next;
		header->next = large;
		large->prev = header;
	}
	return (set_large_block(large, size));
}
