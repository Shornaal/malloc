# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/07 01:27:33 by tiboitel          #+#    #+#              #
#    Updated: 2016/02/24 19:21:51 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
		HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME		 =	libft_malloc_$(HOSTTYPE).so

SRCS 		 = 	malloc.c \
				handler_maps.c \
				realloc.c \
				calloc.c \
				show_alloc_mem.c \
				free.c \
				large_alloc.c

INCLUDES	 =	./includes
SRCSPATH 	 =	./srcs/
LIBFTPATH 	 =  ./libft/
LIBFTINC 	 =  ./libft/includes

CC			 = gcc
CFLAGS		 = -Wall -Werror -Wextra
INCLUDES_O	 = -I $(LIBFTINC) -I $(INCLUDES)
INCLUDES_C	 = -L $(LIBFTPATH) -lft


SRC			 = $(addprefix $(SRCSPATH), $(SRCS))
OBJS		 = $(SRC:.c=.o)

all:		$(NAME)

$(NAME):	$(OBJS)
			$(CC) -shared -o $(NAME) $(OBJS) $(CFLAGS) $(INCLUDES_C)
			@ln -s $(NAME) libft_malloc.so

%.o: %.c libft/libft.a
			$(CC) -o $@ $(INCLUDES_O) -c $<

libft/libft.a:
			make -C $(LIBFTPATH)

clean:
			make -C $(LIBFTPATH) clean
			rm -rf $(OBJS)

fclean: 	clean
			make -C $(LIBFTPATH) fclean
			rm -rf $(NAME)
			rm -rf libft_malloc.so

re: fclean all

.PHONY: clean fclean re
			
