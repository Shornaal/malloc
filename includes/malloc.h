#ifndef FT_MALLOC_H
# define FT_MALLOC_H

#include <stdio.h>
#include <stdlib.h>

#include "libft.h"

#include <sys/mman.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <errno.h>

#define TINY    512
#define SMALL	1500

typedef	enum		e_area_memory
{
	A_TINY, A_SMALL, A_LARGE
}					t_area_mem;

typedef struct		s_maps
{
	void			*tiny;
	void			*small;
	void			*large;
	unsigned int	size_tiny;
	unsigned int	size_small;
	size_t			max_size;
}					t_maps;

typedef struct		s_block
{
	unsigned int	prev;
	size_t			next;
	unsigned char	flag;
}					t_block;

typedef struct		s_header
{
	struct s_header	*next;
	struct s_header	*prev;
	struct s_block	*first;
	void			*end;
	unsigned int	format;
	size_t			size;
}					t_header;

/*
 * 1. MALLOC SECTION.
 */
void	free(void *ptr);
void	*malloc(size_t size);
void	*calloc(size_t count, size_t size);
void	*realloc(void *ptr, size_t size);
void	*large_alloc(size_t size, void **map);
void	free_large(t_header *header, t_block *block);
void	show_alloc_mem(void);

/*
 * 2. HANDLER_MAPS SECTION.
 */
int			create_map(int bsize, void **maps, t_header *prev);
t_header	*create_header(void **map, size_t size, t_header *prev, int bsize);
void		destroy_map(t_header *header);
void		init_maps(void);

void			*create_block(int bsize, size_t size, t_block *block, void *end);
/*
 * 3. HEADER SECTION.
 */
t_header	*get_header_type(t_block *block);
int			check_pointer_integrity(t_header *header, t_block *block);
/* 
 * 4. HANDLE ERROR SECTION. 
 * */
void		set_malloc_error(void);

extern t_maps		g_maps;
#endif
