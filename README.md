UNIX 42 - malloc
================

Some informations:

* In malloc / free implementations, free does normally not return the memory to the operating system. Avoid gaps and OS can only handle memory chunks that the virtual memory manager (VMM) can handledle (multiples of 512 Bytes eg. 4KB)
So, returning 40 Bytes to the OS will just not work, so what does free do ?

* Free put the memory block in its own free blocks. It also tries to meld together adjacent blocks in the address space. Free block list is just a circular list of memory chunks which have of course some data in the beginning. -THIS IS THE REASON WHY MANAGING VERY SMALL MEMORY ELEMENTS- is not efficient with stantard malloc / free. Every memory chunks needs additional data and with smaller sizes more fragmentation happens.

* The free-list is also the first place that malloc looks when a new chunks of memory is needed. When a chunk is found that is bigger then needed memory is just divided in two parts. One is returned to -caller-, other is put back in the free list.

* I'll exist many different optimizations to threat this behaviour but malloc and free must be so universal, standard is aways the fallback when alternatives are not usable. optimizations in handling the free-list (eg. sorted by size) but all they're have own limitations.

EG. of implementations:
-----------------------
1. Get a block of memory form OS through sbrk() (Unix call).
2. Create a header and a footer around that block of memory with some information (size, permissions, next and previous blocks)(circular-list ?)
3. When call to malloc happens, a list is referenced which points to blocks of the appropriate size is avalaible.
4. This block is returned and headers / footers are updated accordingly.

- Please verify create block.
